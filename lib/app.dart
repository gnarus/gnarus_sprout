import 'package:flutter/material.dart';

import 'pages/monitor_page.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Gnarus Monitor Demo',
        // Change primary color to black to match design, this will change the appbar color
        theme: ThemeData(primaryColor: Colors.black),
        home: MonitorPage(title: 'Monitor'),
      );
}
