import 'dart:math';

import 'package:flutter/foundation.dart';
import '../models/record.dart';

class RecordApiService {
  Future<List<Record>> getRecords() async => _createSampleRecords();

  static List<Record> _createSampleRecords() => _createSinSeries(
        dateStart: DateTime.now().subtract(Duration(hours: 1)),
        dateEnd: DateTime.now(),
        centres: [70, 38, 68, 39],
        amplitudes: [5, 2, 3, 4],
      );

  static List<Record> _createSinSeries({
    @required DateTime dateEnd,
    @required DateTime dateStart,
    int numPoints = 60,
    @required List<double> centres,
    @required List<double> amplitudes,
  }) {
    assert(dateStart != null && dateEnd != null);
    assert(centres != null && amplitudes != null);
    assert(centres.isNotEmpty && centres.length == amplitudes.length);

    final result = List<Record>();
    final cycles = 4;
    final totalSeconds = dateEnd.difference(dateStart).inSeconds;
    final increment = Duration(seconds: totalSeconds ~/ numPoints);

    for (var i = dateStart; i.compareTo(dateEnd) < 0; i = i.add(increment)) {
      final seconds = i.difference(dateStart).inSeconds;
      final radians = seconds / totalSeconds * 2 * pi * cycles;

      final values = Map<String, double>();

      for (int j = 0; j < centres.length; j++) {
        final sinOffset = j * 2 * pi / centres.length;
        final sinWave = sin(radians + sinOffset);
        final scaled = sinWave * amplitudes[j];
        final centered = scaled + centres[j];

        values['S${j + 1}'] = centered;
      }

      result.add(Record(timestamp: i.millisecondsSinceEpoch, values: values));
    }

    return result;
  }
}
