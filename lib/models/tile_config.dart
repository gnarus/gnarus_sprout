class TileConfig {
  final String dryBulbSensorName;
  final String wetBulbSensorName;
  final String tileType;

  const TileConfig({
    this.dryBulbSensorName,
    this.wetBulbSensorName,
    this.tileType,
  });
}
