class Record {
  final Map<String, double> values;
  final int timestamp;

  Record({
    this.timestamp,
    this.values,
  });
}
