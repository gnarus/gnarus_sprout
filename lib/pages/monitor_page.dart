import 'package:flutter/material.dart';
import '../models/tile_config.dart';
import '../models/tile_type.dart';
import '../widgets/chart_tile_widget.dart';

class MonitorPage extends StatelessWidget {
  MonitorPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text(this.title)),
        body: Container(
          // Adding padding here to make the screen less squashed
          padding: EdgeInsets.all(16),
          // We are getting an overflow error, because our content is not scrolling
          // I will fix this with a SingleChildScrollView
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  // Match reference spacing
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    'Tunnel Site 1',
                    textAlign: TextAlign.left,
                    // Match reference font
                    style: TextStyle(fontSize: 17),
                  ),
                ),
                // Adding sized boxes here to add space between the elements in the Column
                SizedBox(height: 16),
                ChartTile(
                  title: 'Front Sensor 1',
                  tileConfig: TileConfig(
                    dryBulbSensorName: 'S1',
                    wetBulbSensorName: 'S2',
                    tileType: TileType.Temperature.toString(),
                  ),
                ),
                SizedBox(height: 16),
                ChartTile(
                  title: 'Back Sensor 1',
                  tileConfig: TileConfig(
                    dryBulbSensorName: 'S3',
                    wetBulbSensorName: 'S4',
                    tileType: TileType.Temperature.toString(),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
