import 'dart:async';

import 'package:http/http.dart' as http;

class HttpClient extends http.BaseClient {
  final http.Client _inner = http.Client();

  HttpClient();

  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    request.headers.addAll({
      'Content-type': 'application/json',
      'Accept': 'application/json',
    });

    return _inner.send(request);
  }
}
