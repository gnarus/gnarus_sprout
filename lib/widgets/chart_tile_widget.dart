import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import '../models/record.dart';
import '../models/tile_config.dart';
import '../models/tile_type.dart';
import '../services/record_api_service.dart';
import 'package:intl/intl.dart';

class ChartTile extends StatefulWidget {
  ChartTile({@required this.title, @required this.tileConfig}) {
    assert(this.title != null);
    assert(this.tileConfig != null);
  }

  final TileConfig tileConfig;
  final String title;

  @override
  _ChartTileState createState() => _ChartTileState();

  String _latestTimestamp(List<Record> records) {
    if (records == null || records.isEmpty) {
      return 'n/a';
    }

    final d = DateTime.fromMicrosecondsSinceEpoch(records.last.timestamp);

    return DateFormat('d MMM h:mm').format(d) +
        DateFormat('a').format(d).toLowerCase();
  }

  String _latestValue(List<Record> records, String sensorName) {
    if (records == null || records.isEmpty) {
      return 'n/a';
    }

    final double lastValue = records.last.values[sensorName];

    if (lastValue == null) {
      return 'n/a';
    }

    final String lastValueStr = lastValue.toStringAsFixed(2);

    if (this.tileConfig.tileType == TileType.Temperature.toString()) {
      return '$lastValueStr °C';
    }

    return lastValueStr;
  }

  Widget buildWith(BuildContext context, List<Record> records) => Container(
        // I want to add a decoration to this container so that it has a drop shadow
        decoration: this._containerDecoration,
        child: Column(
          children: <Widget>[
            Container(
              // I have added a bottom border here to separate the header from the chart
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              decoration: BoxDecoration(
                border: Border(bottom: this._separatorBorder),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // Match reference spacing
                  SizedBox(height: 4),
                  Text(
                    this.title,
                    textAlign: TextAlign.left,
                    // Match reference font
                    style: TextStyle(fontSize: 11),
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: <Widget>[
                      // We are now using a customer chip widget to implement the reference design
                      _ChartChip(
                        label: _latestTimestamp(records),
                        backGroundColor: Color(0xFFFFFFFF),
                        borderColor: Color(0xCF000000),
                      ),
                      SizedBox(width: 8),
                      _ChartChip(
                        label: this._latestValue(
                          records,
                          tileConfig.dryBulbSensorName,
                        ),
                        backGroundColor: Color(0x4FF2B0B0),
                        borderColor: Color(0xCFFF0000),
                      ),
                      SizedBox(width: 8),
                      _ChartChip(
                        label: this._latestValue(
                          records,
                          tileConfig.wetBulbSensorName,
                        ),
                        backGroundColor: Color(0x4FB0CFF2),
                        borderColor: Color(0xAF0000FF),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 8, bottom: 4),
              child: _TemperatureChart.fromRecords(
                records: records,
                dryBulbSensorName: this.tileConfig.dryBulbSensorName,
                wetBulbSensorName: this.tileConfig.wetBulbSensorName,
              ),
              height: 250,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.symmetric(horizontal: 16),
              // I have added a top border here to separate the footer from the chart
              decoration: BoxDecoration(
                border: Border(
                  top: this._separatorBorder,
                ),
              ),
              child: DropdownButton(
                // I am leaving this method empty, as choosing an item from the list isn't supported yet by our code
                onChanged: (_) {},
                value: 'ONE HOUR',
                style: TextStyle(fontSize: 12, color: Colors.black),
                // We are now supplying a list of text as input, and then map that list to the widget we want
                // This allows us to define the widget only once
                items: [
                  'ONE HOUR',
                  'ONE DAY',
                  'ONE WEEK',
                  'ALL',
                ]
                    .map(
                      (it) => DropdownMenuItem(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Text(it),
                        ),
                        value: it,
                      ),
                    )
                    .toList(),
                // Here are all the available menu items
              ),
            )
          ],
        ),
      );

  BoxDecoration get _containerDecoration => BoxDecoration(
        color: Colors.white,
        // I have add a thin grey border around the tiles to match the design
        border: Border.all(
          width: 0.2,
          color: Colors.grey,
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 6.0,
            offset: Offset(0, 3),
          )
        ],
      );

  BorderSide get _separatorBorder => BorderSide(
        width: 0.2,
        color: Colors.grey,
      );
}

class _ChartTileState extends State<ChartTile> {
  final recordApiService = RecordApiService();

  List<Record> _records = List<Record>();

  @override
  void initState() {
    super.initState();

    this
        .recordApiService
        .getRecords()
        .then((it) => setState(() => this._records = it));
  }

  @override
  Widget build(BuildContext context) =>
      this.widget.buildWith(context, this._records);
}

class _ChartChip extends StatelessWidget {
  const _ChartChip({
    Key key,
    this.label,
    this.backGroundColor,
    this.borderColor,
  }) : super(key: key);

  final Color backGroundColor;
  final Color borderColor;
  final String label;

  @override
  Widget build(BuildContext context) => Container(
        child: Center(
          child: Text(
            label,
            style: TextStyle(
              fontSize: 10,
              color: this.borderColor,
            ),
          ),
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: this.borderColor,
            width: 0.5,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          color: this.backGroundColor,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 3,
        ),
      );
}

class _TemperatureChart extends StatelessWidget {
  _TemperatureChart(this._seriesList);

  final List<charts.Series> _seriesList;

  static _TemperatureChart fromRecords({
    List<Record> records,
    String dryBulbSensorName,
    String wetBulbSensorName,
  }) {
    final seriesList = [
      new charts.Series<Record, DateTime>(
        id: 'Dry Bulb',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (Record record, _) =>
            DateTime.fromMillisecondsSinceEpoch(record.timestamp),
        measureFn: (Record record, _) => record.values[dryBulbSensorName],
        data: records,
      ),
      new charts.Series<Record, DateTime>(
        id: 'Wet Bulb',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (Record record, _) =>
            DateTime.fromMillisecondsSinceEpoch(record.timestamp),
        measureFn: (Record record, _) => record.values[wetBulbSensorName],
        data: records,
      ),
    ];

    return _TemperatureChart(seriesList);
  }

  charts.DateTimeAxisSpec get _dateTimeAxisSpec => charts.DateTimeAxisSpec(
        tickFormatterSpec: charts.AutoDateTimeTickFormatterSpec(
          minute: new charts.TimeFormatterSpec(
            format: 'd/MM hh:mm',
            transitionFormat: 'd/MM hh:mm',
          ),
        ),
        tickProviderSpec: charts.AutoDateTimeTickProviderSpec(),
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: charts.TextStyleSpec(fontSize: 10),
        ),
      );

  charts.NumericAxisSpec get _measureAxisSpec => charts.NumericAxisSpec(
        showAxisLine: true,
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: charts.TextStyleSpec(fontSize: 10),
        ),
        tickProviderSpec: charts.BasicNumericTickProviderSpec(
            desiredTickCount: 7, zeroBound: false),
      );

  @override
  Widget build(BuildContext context) => new charts.TimeSeriesChart(
        _seriesList,
        domainAxis: this._dateTimeAxisSpec,
        primaryMeasureAxis: this._measureAxisSpec,
      );
}
